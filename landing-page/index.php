<?php
include_once 'model/RegionsModel.php';

$regionModel = new RegionsModel();
$regionArr = $regionModel->getRegions();
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Compre Já</title>
        <meta name="description" content="Compre Já nossos produtos"/>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <style>
            .form{
                margin-top: 30px;
            }
            .form-group{
                margin-bottom: 35px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">

            </div>
            <div class="row" style="margin:30px 0">
                <div class="col-lg-3">
                    <img src="img/logo.png" class="img-thumbnail">
                </div>
                <div class="col-lg-9">
                    <h1>Desafio Casa de Suspense</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <h2>Cadastre e ganhe 10% de desconto no "Desafio da casa de suspense".</h2>
                    <h3>
                        Aconteceu uma morte na Casa de Suspense e você e seus amigos tem que descobrir quem é o assassino.<br/><br/>
                        Um lugar para divertir e quebrar a cabeça. Um jogo emocionante onde o protagonista é você e seus assitentes detetives.<br/><br/>
                        Não perca essa promoção cadastre agora e agende seu horário.<br/><br/>
                        Horários disponíveis todos os dias das 18h às 22h.<br/><br/>
                        Shopping Paulista - Bela Vista <br/><br/>
                        Telefone de Contato: (11) 2338-8788
                    </h3>
                </div>
                <div class="col-lg-6 form" id="form-container">

                    <form id="step_1" action="src/salvar.php" method="post" class="form-step" data-toggle="validator">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        Preencha seus dados para receber contato
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <fieldset>
                                        <div class="row form-group">
                                            <div class="col-lg-12">
                                                <label>Nome Completo</label>
                                                <input class="form-control" type="text" name="nome" pattern=".*[a-zA-ZzáàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ].*[a-zA-ZzáàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ].*" data-error="Nome inválido" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-lg-12">
                                                <label>Data de Nascimento</label>
                                                <input class="form-control" type="text" name="data_nascimento" data-fv-date="true"
                                                       placeholder="DD/MM/YYYY" data-fv-date-format="DD/MM/YYYY" data-fv-date-message="Data inválida!" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-lg-12">
                                                <label>Email</label>
                                                <input class="form-control" type="email" name="email" data-error="Email inválido" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-lg-12">
                                                <label>Telefone</label>
                                                <input class="form-control" type="text" name="telefone" pattern="^\([1-9]{2}\) (?:[2-8][0-9]|9[1-9])[0-9]{2,3}\-[0-9]{4}$" data-error="Telefone inválido" required placeholder="(99) 9999-9999"> 
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div>
                                            <button type="submit" class="btn btn-lg btn-info next-step">Próximo Passo</button>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                    </form>

                    <form id="step_2" class="form-step" style="display:none">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        Preencha seus dados para receber contato
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <fieldset>
                                        <div class="row form-group">
                                            <div class="col-lg-12">
                                                <label>Região</label>
                                                <select class="form-control" name="regiao" required>
                                                    <option value="">Selecione a sua região</option>
                                                    <?php if (isset($regionArr) && is_array($regionArr)) : ?>
                                                        <?php foreach ($regionArr as $region): ?>
                                                            <option value="<?= $region['region_id'] ?>"><?= $region['name'] ?></optio>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-lg-12">
                                                <label>Unidade</label>
                                                <select class="form-control" name="unidade" required>
                                                    <option value="">Selecione a unidade mais próxima</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div>
                                            <button id="enviar" type="submit" class="btn btn-lg btn-info next-step">Enviar</button>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                    </form>

                    <div id="step_sucesso" class="form-step" style="display:none">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    Obrigado pelo cadastro!
                                </div>
                            </div>
                            <div class="panel-body">
                                Em breve você receberá uma ligação com mais informações!
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <script src="js/validator.min.js"></script>
        <script>
            $(function () {
                $('.next-step').click(function (event) {
                    var form = $(this).parents('.form-step');
                    $('.next-step:visible').parents('form').validator().on('submit', function (e) {
                        if (!e.isDefaultPrevented()) {
                          e.preventDefault();
                          form.hide().next().show();
                        }
                      });
                    
                });
                $('#enviar').on('click', function(){
                    $.post('src/salvar.php',{   
                        nome: $("[name=nome]").val(),
                        data_nascimento: $("[name=data_nascimento]").val(),
                        email: $("[name=email]").val(),
                        telefone: $("[name=telefone]").val(),
                        regiao: $("[name=regiao]").val(),
                        unidade: $("[name=unidade]").val()
                        }, 'json');
                });
                $('select[name=regiao]').on('click', function () {
                    var regionId = $(this).val();
                    $.post('src/ajax-unity.php',
                            {regionId: regionId}, function (resultado) {
                        $("select[name=unidade] option").not("[value='']").remove();
                        $.each(resultado, function (value, item) {
                            $("select[name=unidade]").append('<option value="' + item.unit_id + '">' + item.name + '</option>');
                        });
                    }, 'json');
                });
            });
        </script>
    </body>
</html>
