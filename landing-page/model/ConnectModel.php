<?php

class ConnectModel {
    
    private $connection;
    
    public function getConnection(){
        if(!$this->connection){
        
            $this->connection = new PDO(
                'mysql:host=localhost;dbname=actualsales', 'actualsales', '!$@l&51234', 
                    array(
                        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                    )
                );
            
        }
        
        return $this->connection;
    }
    
}

