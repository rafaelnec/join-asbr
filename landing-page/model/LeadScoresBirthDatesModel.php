<?php

include_once 'ConnectModel.php';

class LeadScoresBirthDatesModel extends ConnectModel {
    
    private $table = 'lead_scores_birth_dates';
        
    public function getLeadScoresBirthDatesByDate($date){
         return $this->getConnection()
                     ->query("SELECT * FROM {$this->table} WHERE age_start <= {$date} AND age_end >= {$date}", PDO::FETCH_ASSOC)
                     ->fetch();
    }
    
}

