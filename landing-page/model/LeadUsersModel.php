<?php

include_once 'ConnectModel.php';

class LeadUsersModel extends ConnectModel {
    
    private $table = 'lead_users';
        
    public function insert($dados){
        
        $sth = $this->getConnection()
                     ->prepare("INSERT INTO {$this->table}
                        (`region_id`,
                        `unit_id`,
                        `name`,
                        `email`,
                        `phone_number`,
                        `birth_date`,
                        `total_score`)
                        VALUES
                        (:region_id,
                        :unit_id,
                        :name,
                        :email,
                        :phone_number,
                        :birth_date,
                        :total_score);
                    ");
                     
        $sth->bindValue(':region_id', $dados['region_id']);
        $sth->bindValue(':unit_id', $dados['unit_id']);
        $sth->bindValue(':name', $dados['name']);
        $sth->bindValue(':email', $dados['email']);
        $sth->bindValue(':phone_number', $dados['phone_number']);
        $sth->bindValue(':birth_date', $dados['birth_date']);
        $sth->bindValue(':total_score', $dados['total_score']);
        
        return $sth->execute();
        
    }
    
}

