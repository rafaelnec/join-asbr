<?php

include_once 'ConnectModel.php';

class UnitsModel extends ConnectModel {
    
    private $table = 'units';
        
    public function getUnits($regionId){
         return $this->getConnection()
                     ->query("SELECT * FROM {$this->table} WHERE region_id = {$regionId} ORDER BY name", PDO::FETCH_ASSOC)
                     ->fetchAll();
    }
    
    public function getUnitsById($unitId){
         return $this->getConnection()
                     ->query("SELECT * FROM {$this->table} WHERE unit_id = {$unitId}", PDO::FETCH_ASSOC)
                     ->fetch();
    }
    
}

