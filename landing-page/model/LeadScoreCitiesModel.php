<?php

include_once 'ConnectModel.php';

class LeadScoreCitiesModel extends ConnectModel {
    
    private $table = 'lead_scores_cities';
        
    public function getLeadSocreoCitiesById($unitId){
         return $this->getConnection()
                     ->query("SELECT * FROM {$this->table} WHERE unit_id = {$unitId}", PDO::FETCH_ASSOC)
                     ->fetch();
    }
    
}

