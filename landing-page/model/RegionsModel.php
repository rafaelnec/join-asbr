<?php

include_once 'ConnectModel.php';

class RegionsModel extends ConnectModel {
    
    private $table = 'regions';
        
    public function getRegions(){
         return $this->getConnection()
                     ->query("SELECT * FROM {$this->table} ORDER BY name", PDO::FETCH_ASSOC)
                     ->fetchAll();
    }
    
    public function getRegionsById($regionId){
         return $this->getConnection()
                     ->query("SELECT * FROM {$this->table} WHERE region_id = {$regionId}", PDO::FETCH_ASSOC)
                     ->fetch();
    }
}

