<?php

include_once '../model/RegionsModel.php';
include_once '../model/UnitsModel.php';
include_once '../model/LeadScoreCitiesModel.php';
include_once '../model/LeadScoresBirthDatesModel.php';
include_once '../model/LeadUsersModel.php';

$score = 10;
$dateActual = "2016-06-01";

$name = $_POST['nome'];
$birthDate = $_POST['data_nascimento'];
$email = $_POST['email'];
$phone = str_replace(array("(",")","-", " "), "", $_POST['telefone']);
$regionId = $_POST['regiao'];
$unitId = $_POST['unidade'];
$token = "cbcbe5de207c523a99ce0160141144fe";

if(!empty($name) &&
        !empty($birthDate) &&
        !empty($email) &&
        !empty($phone) &&
        !empty($regionId) &&
        !empty($unitId)){
    
    try{
        
        $leadScoreCitiesModel = new LeadScoreCitiesModel();
        $scoreArr = $leadScoreCitiesModel->getLeadSocreoCitiesById($unitId);
        
        $score += $scoreArr['score'];
        
        $ageTime = strtotime($dateActual) - strtotime(str_replace("/", "-", $birthDate));
        $age = floor($ageTime/31556926);
        
        $leadScoresBirthDatesModel = new LeadScoresBirthDatesModel();
        $scoreArr = $leadScoresBirthDatesModel->getLeadScoresBirthDatesByDate($age);
        
        $score += $scoreArr['score'];
        
        $dateBdArr = array(
            'name' => $name,
            'email' =>  $email,
            'phone_number' => $phone,
            'region_id' => $regionId,
            'unit_id' => $unitId,
            'birth_date' => date("Y-m-d", strtotime(str_replace("/", "-", $birthDate))),
            'total_score' => $score
            
        );
        
        $leadUsersModel = new LeadUsersModel();
        $leadUsersModel->insert($dateBdArr);
        
        $unitsModel = new UnitsModel();
        $unitsArr = $unitsModel->getUnitsById($unitId);
        
        $regionModel = new RegionsModel();
        $regionArr = $regionModel->getRegionsById($regionId);
       
        $dadosServiceArr = array(
            'nome' => $name,
            'email' =>  $email,
            'telefone' => $phone,
            'regiao' => $regionArr['name'],
            'unidade' => $unitsArr['name'],
            'data_nascimento' => date("Y-m-d", strtotime(str_replace("/", "-", $birthDate))),
            'score' => $score,
            'token' => $token
            
        );
        
        if (!function_exists("curl_version")) {
            throw new Exception("Curl não instalado!");
        }
        
        $ch = curl_init("http://api.actualsales.com.br/join-asbr/ti/lead");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($dadosServiceArr));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (!$resultado = curl_exec($ch)) {
            trigger_error(curl_error($ch));
        }
        curl_close($ch);
        
    } catch (Exception $ex) {

        var_dump($ex->getMessage());
        
    }
    
}