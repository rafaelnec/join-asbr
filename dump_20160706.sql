CREATE DATABASE  IF NOT EXISTS `actualsales` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `actualsales`;
-- MySQL dump 10.13  Distrib 5.7.12, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: actualsales
-- ------------------------------------------------------
-- Server version	5.7.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lead_scores_birth_dates`
--

DROP TABLE IF EXISTS `lead_scores_birth_dates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lead_scores_birth_dates` (
  `lead_score_birth_date_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `age_start` int(11) NOT NULL,
  `age_end` int(11) DEFAULT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (`lead_score_birth_date_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lead_scores_birth_dates`
--

LOCK TABLES `lead_scores_birth_dates` WRITE;
/*!40000 ALTER TABLE `lead_scores_birth_dates` DISABLE KEYS */;
INSERT INTO `lead_scores_birth_dates` VALUES (1,100,99999,-5),(2,0,17,-5),(3,40,99,-3),(4,18,39,0);
/*!40000 ALTER TABLE `lead_scores_birth_dates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lead_scores_cities`
--

DROP TABLE IF EXISTS `lead_scores_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lead_scores_cities` (
  `lead_score_city_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unit_id` int(10) unsigned NOT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (`lead_score_city_id`),
  KEY `fk_lead_scores_cities_1_idx` (`unit_id`),
  CONSTRAINT `fk_lead_scores_cities_1` FOREIGN KEY (`unit_id`) REFERENCES `units` (`unit_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lead_scores_cities`
--

LOCK TABLES `lead_scores_cities` WRITE;
/*!40000 ALTER TABLE `lead_scores_cities` DISABLE KEYS */;
INSERT INTO `lead_scores_cities` VALUES (1,1,-2),(2,2,-2),(3,3,0),(4,4,-1),(5,5,-1),(6,6,-3),(7,7,-4),(8,8,-4),(9,9,-5);
/*!40000 ALTER TABLE `lead_scores_cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lead_users`
--

DROP TABLE IF EXISTS `lead_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lead_users` (
  `lead_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` int(10) unsigned NOT NULL,
  `unit_id` int(10) unsigned NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone_number` varchar(11) NOT NULL,
  `birth_date` date NOT NULL,
  `total_score` int(11) NOT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lead_user_id`),
  KEY `fk_lead_users_1_idx` (`region_id`),
  KEY `fk_lead_users_2_idx` (`unit_id`),
  CONSTRAINT `fk_lead_users_1` FOREIGN KEY (`region_id`) REFERENCES `regions` (`region_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_lead_users_2` FOREIGN KEY (`unit_id`) REFERENCES `units` (`unit_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lead_users`
--

LOCK TABLES `lead_users` WRITE;
/*!40000 ALTER TABLE `lead_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `lead_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regions`
--

DROP TABLE IF EXISTS `regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regions` (
  `region_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `slug` varchar(45) NOT NULL,
  PRIMARY KEY (`region_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regions`
--

LOCK TABLES `regions` WRITE;
/*!40000 ALTER TABLE `regions` DISABLE KEYS */;
INSERT INTO `regions` VALUES (1,'Sul','sul'),(2,'Sudeste','sudeste'),(3,'Centro-Oeste','centro-oeste'),(4,'Nordeste','nordeste'),(5,'Norte','norte');
/*!40000 ALTER TABLE `regions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units` (
  `unit_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `slug` varchar(45) NOT NULL,
  PRIMARY KEY (`unit_id`),
  KEY `fk_cities_1_idx` (`region_id`),
  CONSTRAINT `fk_cities_1` FOREIGN KEY (`region_id`) REFERENCES `regions` (`region_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units`
--

LOCK TABLES `units` WRITE;
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
INSERT INTO `units` VALUES (1,1,'Porto Alegre','porto-alegre'),(2,1,'Curitiba','curitiba'),(3,2,'São Paulo','sao-paulo'),(4,2,'Rio de Janeiro','rio-de-janeiro'),(5,2,'Belo Horizonte','belo-horizonte'),(6,3,'Brasília','brasilia'),(7,4,'Salvador','salvador'),(8,4,'Recife','recife'),(9,5,'INDISPONÍVEL','indisponivel');
/*!40000 ALTER TABLE `units` ENABLE KEYS */;
UNLOCK TABLES;

CREATE USER 'actualsales'@'localhost' IDENTIFIED BY '!$@l&51234';
GRANT USAGE ON *.* TO 'actualsales'@'localhost';                      
GRANT ALL PRIVILEGES ON `actualsales`.* TO 'actualsales'@'localhost';
FLUSH PRIVILEGES;

--
-- Dumping events for database 'actualsales'
--

--
-- Dumping routines for database 'actualsales'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-07  2:23:29
